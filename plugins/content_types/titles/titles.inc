<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('SubQueue title'),
  'no title override' => FALSE,
  'defaults' => array('qid' => NULL),
  'icon' => 'node.png',
  'description' => t('NodeQueue SubQueue title, for this specific taxonomy term.'),
  'category' => t('Taxonomy term'),
  'edit text' => t('Configure'),
);

/**
 * Render the custom content type.
 */
function subqueue_titles_titles_content_type_render($subtype, $conf, $panel_args, $context) {  
  // Get the tid
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $tid = arg(2);
    $title = subqueue_titles_get($tid, $conf['qid']);
  }

  $block = new stdClass();
  $block->title = '';
  $block->content  = '<h4 class="pane-title">' . t($title) . '</h4>';
  
  return $block;
}

/**
 * 'Edit form' callback for the content type.
 */
function subqueue_titles_titles_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf']; // Shorthand.
  
  // Get all the taxonomy smartqueues.
  $result = subqueue_titles_get_queues();
  $options = array();
  foreach ($result as $row) {
    $options[$row->qid] = t($row->title);
  }

  // provide a blank form so we have a place to have context setting.
  $form['qid'] = array(
    '#type' => 'select', 
    '#title' => t('Position'),
    '#options' => $options,
    '#default_value' => $conf['qid'] ? $conf['qid'] : NULL,
  );
  
  return $form;
}

/**
 * Submit handler for settings form.
 */
function subqueue_titles_titles_content_type_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Returns the administrative title for a type.
 */
function subqueue_titles_titles_content_type_admin_title($subtype, $conf, $context) {
  return t('SubQueue title');
}

